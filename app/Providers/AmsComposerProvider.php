<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AmsComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('pageTitle', 'Clean Blog - Common Title');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
